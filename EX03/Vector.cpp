#include "Vector.h"


/*
	Constuctor
	Input:
		n - the initial size of the vector
	Output:
		None
*/
Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	this->_size = 0;
	this->_capacity = n;
	this->_resizeFactor = n;
	this->_elements = new int[n];
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Vector::~Vector()
{
	if (this->_elements != nullptr)
	{
		delete[] this->_elements;
		this->_elements = nullptr;
	}
}


/*
	Function returns the size of the vector
	Input:
		None
	Output:
		size of the vector
*/
int Vector::size() const
{
	return this->_size;
}


/*
	Function returns the capacity of the vector
	Input:
		None
	Output:
		capacity
*/
int Vector::capacity() const
{
	return this->_capacity;
}


/*
	Function return the resize factor
	Input:
		None
	Output:
		resize factor
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}


/*
	Function checks if vector is empty
	Input:
		None
	Output:
		True if vector is empty, False if its not
*/
bool Vector::empty() const
{
	return this->_size == 0;
}


/*
	Function adds a value to the end of the vector
	If the vector is full, we will assign new capacity to the vector(capacity + resizeFactor)
	Input:
		value that needs to be added to the end of the vector
	Output:
		None
*/
void Vector::push_back(const int& val)
{
	int i = 0;
	//declaring vars
	if (this->_size < this->_capacity)
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else
	{
		this->_capacity += this->_resizeFactor;
		int *newArray = new int[this->_capacity];
		for (i = 0; i < this->_size; i++)
		{
			newArray[i] = this->_elements[i];
		}
		newArray[this->_size] = val;
		this->_size++;
		delete[] this->_elements;
		this->_elements = newArray;
	}
}


/*
	Function deletes the last value in the vector and returns it
	If vector is empty, print error message and return -9999
	Input:
		None
	Output:
	the last value in the vector
	If vector is empty, return -9999
*/
int Vector::pop_back()
{
	int lastValue = 0;
	//declaring vars
	if (empty())
	{
		std::cout << "error: pop from empty vector";
		return -9999;
	}
	this->_size--;
	lastValue = this->_elements[this->_size];
	return lastValue;
}


/*
	Function makes sure that the capacity will be at least n
	Input:
		reserved capacity
	Output:
		None
*/
void Vector::reserve(int n)
{
	int i = 0;
	//declaring vars
	if (this->_capacity < n)
	{
		while (this->_capacity < n)
		{
			this->_capacity += this->_resizeFactor;
		}
		int *newArray = new int[this->_capacity];
		for (i = 0; i < this->_size; i++)
		{
			newArray[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = newArray;
	}
}

/*
	Function changes the size of the array to n
	Input:
		new size of array
	Output:
		None
*/
void Vector::resize(int n)
{
	if (n <= this->_capacity)
	{
		this->_size = n;
	}
	else
	{
		reserve(n);
	}
}


/*
	Function assigns a value to all available vector elements
	Input:
		value to assign to all available vector elements
	Output:
		None
*/
void Vector::assign(int val)
{
	int i = 0;
	//declaring vars
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}


/*
	Function changes size of an array and adds val to every new element
	Input:
		new size, value to add to every new element
	Output:
		None
*/
void Vector::resize(int n, const int& val)
{
	int i = 0;
	//declaring vars
	resize(n);
	for (i = this->_size; i < this->_capacity; i++)
	{
		this->_elements[i] = val;
	}
}


/*
	Function copies a vector
	Input:
		other vector
	Output:
		None
*/
Vector::Vector(const Vector& other)
{
	int i = 0;
	//declaring vars
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_elements = new int[this->_size];
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}


/*
	Function copies a vector and returns the new vector
	Input:
		other vector
	Output:
		new vector
*/
Vector& Vector::operator=(const Vector& other)
{
	int i = 0;
	//declaring vars
	if (this == &other)
	{
		return *this;
	}
	delete[] this->_elements;
	this->_capacity = other.capacity();
	this->_resizeFactor = other.resizeFactor();
	this->_size = other.size();
	this->_elements = new int[this->_capacity];
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}


/*
	Function gets a index and returns the value thats in the index
	input:
		index
	Output:
		number in index
*/
int& Vector::operator[](int n) const
{
	if (n > this->_size - 1 || n < 0)
	{
		std::cout << "Invalid index\n";
		return this->_elements[0];
	}
	return this->_elements[n];
}